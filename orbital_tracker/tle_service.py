from typing import List, AsyncIterator
from logging import getLogger
from aiocache import cached
from skyfield.api import EarthSatellite, load
import httpx


log = getLogger(__name__)
url = (
    "https://www.celestrak.com/NORAD/elements/gp.php?GROUP=weather&FORMAT=tle"
)


class TLEService:
    @classmethod
    async def _tle_sats(cls) -> AsyncIterator[EarthSatellite]:
        timescale = load.timescale()
        async with httpx.AsyncClient() as client:
            async with client.stream("GET", url) as response:
                response.raise_for_status()
                lines = response.aiter_lines()

                while True:
                    try:
                        name = await anext(lines)
                        line1 = await anext(lines)
                        line2 = await anext(lines)
                        yield EarthSatellite(line1, line2, name, timescale)
                    except StopAsyncIteration:
                        break

    @classmethod
    @cached()
    async def satellites(cls) -> List[EarthSatellite]:
        return [sat async for sat in cls._tle_sats()]

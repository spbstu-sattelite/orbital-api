"""Api client dependency container."""
from dependency_injector import containers, providers
import zerorpc
from .api import OrbitalApi


class OrbitalApiClient(containers.DeclarativeContainer):
    config = providers.Configuration()
    client_class = providers.Dependency(instance_of=OrbitalApi)
    client: OrbitalApi = zerorpc.Client()

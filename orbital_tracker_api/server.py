"""Api server dependency container."""
from dependency_injector import containers, providers
import zerorpc
from .api import OrbitalApi


class OrbitalApiServer(containers.DeclarativeContainer):
    config = providers.Configuration()
    server_class = providers.Dependency(instance_of=OrbitalApi)
    server = zerorpc.Server(server_class())
